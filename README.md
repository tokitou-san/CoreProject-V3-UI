<div align="center">

  <h2 align="center">ANIMECORE V3 UI</h2>

  <p align="center">
   <b>A modern anime streaming platform under CoreProject
     <br>
   This is the V3 UI for <a href="https://github.com/baseplate-admin/CoreProject/">CoreProject</a> / <a href="https://github.com/baseplate-admin/CoreProject-V3-UI/">AnimeCore<a/>
     <br>
     check <a href="https://coreproject.moe/anime/">v2 UI</a>
    <br><br>
    <a href="https://github.com/baseplate-admin/CoreProject"><strong>Explore the docs »</strong></a>
  </p>
</div>

<p align="center">
  <a href="https://github.com/baseplate-admin/CoreProject-V3-UI/graphs/contributors" alt="Contributors">
    <img src="https://img.shields.io/github/contributors/baseplate-admin/CoreProject-V3-UI.svg?style=for-the-badge" >
  </a>
  <a href="https://github.com/baseplate-admin/CoreProject-V3-UI/network/members" alt="Forks">
    <img src="https://img.shields.io/github/forks/baseplate-admin/CoreProject-V3-UI.svg?style=for-the-badge">
  </a>
  <a href="https://github.com/baseplate-admin/CoreProject-V3-UI/issues" alt="Issues">
    <img src="https://img.shields.io/github/issues/baseplate-admin/CoreProject-V3-UI.svg?style=for-the-badge">
  </a>
  <a href="https://github.com/baseplate-admin/CoreProject-V3-UI/blob/v2/LICENSE" alt="License - AGPL-3.0">
    <img src="https://img.shields.io/github/license/baseplate-admin/CoreProject-V3-UI.svg?style=for-the-badge">
  </a>

  <img alt="GitHub code size in bytes" src="https://img.shields.io/github/languages/code-size/baseplate-admin/CoreProject-V3-UI?style=for-the-badge">
  <img alt="Lines of code" src="https://img.shields.io/tokei/lines/github/baseplate-admin/CoreProject-V3-UI?style=for-the-badge">
  <a href='https://discord.gg/7AraSmKqnN'><img alt="Discord" src="https://img.shields.io/discord/1039894823626362931?style=for-the-badge"></a>
</p>
    
## Screenshots

<p float="left">
  <i>Click on the images to open full view in new tab</i>
  <br>
  <br>
  <img src="https://github.com/tokitou-san/CoreProject-V3-UI/assets/114811070/9107b245-d770-4050-af31-6d1f16559ead" alt="Home Page Mockup" width=49%>
  <img src="https://github.com/tokitou-san/CoreProject-V3-UI/assets/114811070/6e72e66f-aa1b-49b9-85d6-2c86b21c4c98" alt="Anime Info Page Mockup" width=49%>
  <img src="https://github.com/tokitou-san/CoreProject-V3-UI/assets/114811070/3f21b783-f511-418d-8f7c-375bef81afde" alt="Register Page Mockup" width=49%>
  <img src="https://github.com/tokitou-san/CoreProject-V3-UI/assets/114811070/04a82f6a-11db-4235-a4fd-816afcadb736" alt="Search Panel Mockup" width=49%>

## Contributing

-   If you have a suggestion/idea that would make this project better, please create a pull request. All pull requests will be reviewed by us, and adjusted.

-   You can also [open a new issue](https://github.com/baseplate-admin/CoreProject-V3-UI/issues/new/choose) or [help us with an existing one](https://github.com/baseplate-admin/CoreProject-V3-UI/issues).

Other than that, you can also help the project by giving it a star! Your help is extremely appreciated :)

## License

Distributed under the AGPL-3.0 License. See [`LICENSE`](https://github.com/baseplate-admin/CoreProject-V3-UI/blob/v2/LICENSE) for more information.
