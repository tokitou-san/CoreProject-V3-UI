export const latest_episodes = [
    {
        id: 1,
        name: "SpyxFamily",
        cover: "/images/SpyxFamily.avif",
        episode_number: 6,
        release_date: "2023-04-22T10:30:00.000Z"
    },
    {
        id: 2,
        name: "Kaguya-sama: Love Is War",
        cover: "/images/KaguyaSama.avif",
        episode_number: 5,
        release_date: "2023-04-22T10:30:00.000Z"
    },
    {
        id: 3,
        name: "Aharen-san wa Hakaraenai",
        cover: "/images/AharenSan.avif",
        episode_number: 9,
        release_date: "2023-04-22T11:30:00.000Z"
    },
    {
        id: 4,
        name: "Summer time Rendering",
        cover: "/images/SummerTimeRendering.avif",
        episode_number: 12,
        release_date: "2023-04-22T12:30:00.000Z"
    }
];
